# Idena Kickstarter Proposals

<!-- If you can read this comment. View the file here : https://gitlab.com/EarthlingDavey/idena-kickstarter-proposals/-/blob/master/README.md to see the correct formatting. -->

## Directory structure

Vote/donate address is a file. So it cen be read via GitLab graphql api directly in browser.

## How to add a proposal

### Proposal

1.  Create a folder with lowercase and - only. This will be part of the url on the kickstarter website.

    e.g. `/free-hugs`

1.  Your description for the proposal should be in a markdown file with the same name.

    e.g. `/free-hugs/free-hugs.md`

1.  At the start of this file will be the proposal data.

    Add title and date like this:

    <!-- Don't copy ```s -->

    ```
    ---
    title: Free hugs for all humans
    date: 2020.04.13
    ---
    ```

1.  Use markdown to introduce your project here

    e.g.

     <!-- Don't copy ```s -->

    ```
    # Hugs

    Marzipan brownie biscuit wafer chocolate bar carrot cake. Brownie tart tart topping. Cake chocolate bar biscuit ice cream tart. Macaroon toffee cheesecake gummi bears cupcake sesame snaps chocolate jelly-o.

    ## Planning

    Cheesecake icing cheesecake cookie cotton candy bonbon. Marzipan halvah chocolate cake topping. Cupcake jujubes fruitcake cake. Wafer oat cake toffee fruitcake jelly beans gingerbread tiramisu biscuit bear claw.

    ## Research

    Fruitcake tart jujubes tart cookie croissant. Sugar plum biscuit brownie cheesecake danish cookie sweet cheesecake macaroon. Brownie sesame snaps bear claw. Candy canes danish carrot cake dragée lemon drops candy.
    ```

### Voting address

An admin or escrow will create your address for votes and donations

It will look like this:

e.g. `free-hugs/0x49c20b95e62e1fc2a3a27dfcf1ca84739b6e0f8b`

### Milestones

1.  Every project must have a milestone.

    Milestones are an indicator of what you will achieve. And, each proposal may have multiple milestones.

    e.g. `/free-hugs/ms.1.md`, `/free-hugs/ms.2.md`

1.  Add data to the milestone file. Add your title and your address where you would like to be paid. Followed by your target DNA amount

    e.g.

    <!-- Don't copy ```s -->

    ```
    ---
    title: Hugs for 10 people
    payouts:
      - 0xF2B4F700D2975AbD39000587f9788f66AFEDF691:2
    ---
    ```

1.  Add your description to the milestone file.

    e.g.

    <!-- Don't copy ```s -->

    ```
    ## 10 Hugs

    I'll hug 10 people, fund me for that. I will squeeze them extra tight.
    ```

### Images

1.  Illustrate your markdown files with images.

    Create an images folder for your proposal galled `images` e.g. `/free-hugs/images`.

    Add an image to the images folder e.g. `/free-hugs/images/big-hug.jpg`

    And reference it inside your markdown like this

    e.g.

    <!-- Don't copy `s -->

    `![Panda alt text](images/big-hug.jpg 'Panda Title Text')`

### Translations

Coming soon
