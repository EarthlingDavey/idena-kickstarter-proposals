---
# A short title for the milestone
title: First milestone
# Your Idena address followed by the amount of DNA you want to raise
# Use the format <your address>:<DNA amount>
# This is not the voting address,
# it is the address where you will be paid after the work is complete
payouts:
  - 0xF2B4F700D2975AbD39000587f9788f66AFEDF691:1.25
---

### First milestone

If you are working on a small project, write about the project targets here.

Or, if you are working on a multi-part project, create another milestone in the came format
call it `ms.2.md` and use the same format as this document.
